# Project Name: Julia Language Profile Analysis
# Regular Imports
using ProjectFlow
using JSON
using DataFrames

# Data Is not Needed for Now
# using Dates

# Project Related Import
# using TwiliteTimeline

p = Project(
    id="1234",
    name="Twitter Profile Analysis",
    template="jl",
    profile="default"
)

datalake, idata, iviz = initiate(p);


tweets = []
open(joinpath(datalake, "tweets.json"), "r") do f
    global tweets
    dicttxt = read(f,String)  # file information to string
    tweets=JSON.parse(dicttxt)  # parse and transform data
end;



field_map = [
    "id_str",
    "text",
    "entities",
    "retweet_count",
    "favorite_count",
    "created_at"
]


function to_dict_of_array(tws::Array, fields::Array)
    doa = Dict(Symbol(field) => Array{Any}(undef, length(tws)) for field in fields)
    for (i, tweet) in enumerate(tws)
        for fn in fields
            sym_fn = Symbol(fn)
            if fn == "entities"
                hashtags = get(tweet[fn], "hashtags", Dict())
                if length(hashtags) > 0
                    doa[sym_fn][i] = [get(tag, "text", "") for tag in hashtags]
                else
                    doa[sym_fn][i] = []
                end
            else
                doa[sym_fn][i]  = tweet[fn]
            end
        end
    end
    return doa
end

new_tweets = to_dict_of_array(tweets, field_map);
tweets_df = DataFrame(new_tweets)
tweets_df[!,:retweet_count] = convert.(Integer, tweets_df[:, :retweet_count]);
tweets_df[!,:favorite_count] = convert.(Integer, tweets_df[:, :favorite_count]);
tweets_df[!,:id_str] = convert.(String, tweets_df[:, :id_str]);
