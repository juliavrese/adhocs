# Project Name: Julia Language Profile Analysis
# Regular Imports
using ProjectFlow
using JSON
using DataFrames

# Data Is not Needed for Now
using Dates

# Project Related Import
# using TwiliteTimeline

p = Project(
    id="1234",
    name="Twitter Profile Analysis",
    template="jl",
    profile="default"
)

datalake, idata, iviz = initiate(p);



tweets = []
open(joinpath(datalake, "tweets.json"), "r") do f
    global tweets
    dicttxt = read(f,String)
    tweets=JSON.parse(dicttxt)
end;

length(tweets)

struct Tweet
    id_str::String
    text::String
    entities::Array{Union{String, Nothing}}
    retweet_count::Integer
    favorite_count::Integer
    created_at::DateTime
end

function to_dict_of_array(ta::Array; fn::Array, ft::Array)
    dtf = @dateformat_str "e u d HH:MM:SS +s yyyy"
    name_type = zip(fn, ft)
    doa = Dict(name_ => Array{type_}(undef, length(ta)) for (name_, type_) in zip(fn, ft))
    for (i, tweet) in enumerate(ta)
        for (fn, ft) in name_type
            fn_str = string(fn)
            if fn_str == "entities"
                hashtags = get(tweet[fn_str], "hashtags", Dict())
                if length(hashtags) > 0
                    doa[fn][i] = [get(tag, "text", "") for tag in hashtags]
                else
                    doa[fn][i] = []
                end
            elseif fn_str == "created_at"
                doa[fn][i] = parse(DateTime, tweet[fn_str], dtf)
            else
                doa[fn][i]  = tweet[fn_str]
            end
        end
    end
    return doa
end

field_name = [name for name in fieldnames(Tweet)]
field_type = [type for type in fieldtypes(Tweet)]
new_tweets = to_dict_of_array(tweets, fn=field_name, ft=field_type);

# Saving Tweets into Json
open(joinpath(datalake, "tweets_processed.json"), "w") do f
    JSON.print(f, new_tweets)
end
