# Project Name: Julia Language Profile Analysis
# Regular Imports
using ProjectFlow
using JSON
using DataFrames
using CSV
using Dates
using Gadfly
using Distributions
Gadfly.push_theme(:dark)


p = Project(
    id="1234",
    name="Twitter Profile Analysis",
    template="jl",
    profile="default"
)

datalake, iviz, idata = initiate(p);


function read_to_df(p::String)
    data_dict = Dict()
    open(p, "r") do f
        dicttxt = read(f,String)
        data_dict=JSON.parse(dicttxt)
    end
    return DataFrame(data_dict)
end

tweets_df = read_to_df(joinpath(datalake, "tweets_processed.json"));

dtf = @dateformat_str "yyyy-m-dTHH:MM:SS"
parse_date(dt::String) =  parse(DateTime, dt, dtf)

tweets_df[!,:retweet_count] = convert.(Integer, tweets_df[:, :retweet_count]);
tweets_df[!,:favorite_count] = convert.(Integer, tweets_df[:, :favorite_count]);
tweets_df[!,:id_str] = convert.(String, tweets_df[:, :id_str]);

# Apply Custom Functions
tweets_df[!, :created_at] = parse_date.(tweets_df[:, :created_at]);

# Recollect within an array
tweets_df[!, :entities] = collect(Array, tweets_df[:, :entities]);

# Show All Columns DataFrame
show(tweets_df[[1, 2], :], true)

# Top Peforming posts?
tweets_df[!, :total_interaction] = .+(tweets_df[:, :retweet_count], tweets_df[:, :favorite_count]);
top_6_posts = first(sort(tweets_df, [:total_interaction], rev=true), 6)
show(top_6_posts, true)

# writting the file
CSV.write(joinpath(idata, "top_tweets.csv"), select(top_6_posts, Not(:entities)))


# Most Popular Hash Tags?
all_tags_df = DataFrame(
    Dict("tags" => lowercase.(vcat(tweets_df[:, :entities]...)))
);

tags_grouped_df = combine(groupby(all_tags_df[:, [:tags]], [:tags]), nrow => :count)
top_5_tags = first(sort(tags_grouped_df, [:count], rev=true), 5)

plot(
    top_5_tags, x="tags", y="count", Geom.bar,
    style(bar_spacing=2mm),
    Guide.XLabel("Hashtags"),
    Guide.YLabel("Count"),
    Guide.Title("Top #Hashtag Usage"),
)

# Check the Distribution of Interaction?
plot(tweets_df, x="total_interaction", Geom.histogram)


# Fitting A Distribution?
desc = describe(tweets_df[:, [:total_interaction]], :max, :mean)

model = fit(LogNormal, tweets_df[:, :total_interaction])

sample_value = rand(model, 10000)

# Plotting Both
plot(
    layer(tweets_df, x="total_interaction", Geom.density, Theme(default_color=colorant"green")),
    layer(x=sample_value, Geom.density, Theme(default_color=colorant"red")),
    Guide.XLabel("Total Interactions"),
    Guide.YLabel("KDE"),
    Guide.Title("Actual vs Fitted(Log-Normal)"),
    Guide.manual_color_key("Legend", ["Actual", "Fitted"], ["green", "red"])
)

# Check If there is any posts can be considered as Anomaly?
