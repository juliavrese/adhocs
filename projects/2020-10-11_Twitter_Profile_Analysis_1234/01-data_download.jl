# Project Name: Julia Language Profile Analysis
# Regular Imports
using ProjectFlow
using JSON

# Project Related Import
using TwiliteTimeline

p = Project(
    id="1234",
    name="Twitter Profile Analysis",
    template="jl",
    profile="default"
)

datalake, idata, iviz = initiate(p);

# Create value of type Authentictor
creds = Authentictor(ENV["CKEY"], ENV["CSEC"], ENV["OTOK"], ENV["OSEC"]);

# Create Value of Type ResourceParams
tw_params = ResourceParams("JuliaLanguage", count=500, trim_user=true, exclude_replies=true)

# Call The Twitter API
tweets = collect_tweets(creds, tw_params);

# Checking The Length of Tweets
println(length(tweets))

# Saving Tweets into Json
open(joinpath(datalake, "tweets.json"), "w") do f
    JSON.print(f, tweets)
end

open(joinpath(datalake, "tweets_1.json"), "w") do f
    JSON.print(f, tweets[1])
end
