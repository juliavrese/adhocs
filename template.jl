using ProjectFlow

p = ProjectFlow.Project(
    id="1234",
    name="Twitter Profile Analysis",
    template="jl",
    profile="default"
)

datalake, iviz, idata = initiate(p)
